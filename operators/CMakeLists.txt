# Set source files
set(srcs
  Registrar.cxx
  ReadResourceManagerState.cxx
  WriteResourceManagerState.cxx
)

# Set header files
set(headers
  Registrar.h
  ReadResourceManagerState.h
  WriteResourceManagerState.h
)

# Declare the library
add_library(smtkReadWriteResourceManagerState
 ${srcs}
)

target_link_libraries(smtkReadWriteResourceManagerState
  LINK_PUBLIC
    smtkCore
    Boost::boost
    Boost::filesystem
)

target_include_directories(smtkReadWriteResourceManagerState PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
)

generate_export_header(smtkReadWriteResourceManagerState EXPORT_FILE_NAME Exports.h)

smtk_operation_xml("${CMAKE_CURRENT_SOURCE_DIR}/ReadResourceManagerState.sbt" operatorXML)
smtk_operation_xml("${CMAKE_CURRENT_SOURCE_DIR}/WriteResourceManagerState.sbt" operatorXML)

smtk_get_kit_name(name dir_prefix)

# Install the header files
install(
  FILES
    ${headers}
    ${CMAKE_CURRENT_BINARY_DIR}/Exports.h
  DESTINATION
    include/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})

# Install the library and exports
install(
  TARGETS smtkReadWriteResourceManagerState
  EXPORT  ReadWriteSMTKResourceManagerState
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin
  PUBLIC_HEADER DESTINATION include/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})
